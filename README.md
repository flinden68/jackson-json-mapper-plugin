# Jackson JSON wrapper plugin #

For more details about the Jackson library, see http://jackson.codehaus.org/

In this repository I have put all the code. The plugin project, feature project and the update site

#getting started
Download the update site and point your update site database to the site.xml to import the plugin in the update site.
And Go..
In the xsp.properties of the nsf select the "nl.elstarit.jackson.XspLibrary" at the Page generation tab



#read value from JSON string to a POJO object
JacksonWrapper wrapper = new JacksonWrapper();

POJOObject object = (POJOObject)wrapper.readValueFromString(json, POJOObject.class);

#read value from JSON file to a POJO object
JacksonWrapper wrapper = new JacksonWrapper();

POJOObject object = (POJOObject)wrapper.readValueFromFile(json, POJOObject.class);

#read value from JSON InputStream to a POJO object
JacksonWrapper wrapper = new JacksonWrapper();

POJOObject object = (POJOObject)wrapper.readValueFromInputStream(json, POJOObject.class);

#read value from JSON string to a collection<POJO object>
JacksonWrapper wrapper = new JacksonWrapper();

POJOObject object = (POJOObject)wrapper.readValueFromString(json, List.class, POJOObject.class);

#read value from JSON file to a collection<POJO object>
JacksonWrapper wrapper = new JacksonWrapper();

POJOObject object = (POJOObject)wrapper.readValueFromFile(json, List.class, POJOObject.class);

#read value from JSON InputStream to a collection<POJO object>
JacksonWrapper wrapper = new JacksonWrapper();

POJOObject object = (POJOObject)wrapper.readValueFromInputStream(json, List.class, POJOObject.class);

#write value from POJO object to JSON string
JacksonWrapper wrapper = new JacksonWrapper();

String json = wrapper.writeValueAsString(POJOObject(s));

#write value from POJO object to JSON string, but nice formatted
JacksonWrapper wrapper = new JacksonWrapper();

String json = wrapper.writeWithDefaultPrettyPrinter(POJOObject(s));


#write value from POJO object to an OutputStream
JacksonWrapper wrapper = new JacksonWrapper();

wrapper.writeValueToOutputstream(POJOObject(s));

#write value from POJO object to an FileWriter
JacksonWrapper wrapper = new JacksonWrapper();

wrapper.writeValueToFilewriter(POJOObject(s));

#write value from POJO object to an File
JacksonWrapper wrapper = new JacksonWrapper();

wrapper.writeValueToFile(POJOObject(s));
