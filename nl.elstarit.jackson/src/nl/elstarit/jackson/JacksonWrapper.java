package nl.elstarit.jackson;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JacksonWrapper {

	private ObjectMapper mapper = null;
	private String result = null;
	private Object clazz = null;

	public JacksonWrapper(final boolean datesAsTimeStamps){
		mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, datesAsTimeStamps);
	}

	public JacksonWrapper(){
		mapper = new ObjectMapper();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object readCollectionValueFromString(final String obj, final Class<?> collection, final Class<?> element) throws PrivilegedActionException{
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			@Override
			public Object run() throws Exception {
				JacksonWrapper.this.readCollectionFromStringImpl(obj, collection, element);
				return null;
			}
		});
		return clazz;
	}

	@JsonAnySetter
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object readCollectionValueFromFile(final File obj, final Class<?> collection, final Class<?> element) throws PrivilegedActionException{
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			@Override
			public Object run() throws Exception {
				JacksonWrapper.this.readCollectionFromFileImpl(obj, collection, element);
				return null;
			}
		});
		return clazz;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object readCollectionValueFromInputStream(final InputStream obj, final Class<?> collection, final Class<?> element) throws PrivilegedActionException{
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			@Override
			public Object run() throws Exception {
				JacksonWrapper.this.readCollectionFromInputStreamImpl(obj, collection, element);
				return null;
			}
		});
		return clazz;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object readValueFromString(final String obj, final Class<?> cls) throws PrivilegedActionException{
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			@Override
			public Object run() throws Exception {
				JacksonWrapper.this.readValueFromStringImpl(obj, cls);
				return null;
			}
		});
		return clazz;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object readValueFromFile(final File obj, final Class<?> cls) throws PrivilegedActionException{
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			@Override
			public Object run() throws Exception {
				JacksonWrapper.this.readValueFromFileImpl(obj, cls);
				return null;
			}
		});
		return clazz;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object readValueFromInputStream(final InputStream obj, final Class<?> cls) throws PrivilegedActionException{
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			@Override
			public Object run() throws Exception {
				JacksonWrapper.this.readValueFromInputStreamImpl(obj, cls);
				return null;
			}
		});
		return clazz;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String writeValueAsString(final Object obj) throws PrivilegedActionException{
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			@Override
			public Object run() throws Exception {
				JacksonWrapper.this.writeValueAsStringImpl(obj);
				return null;
			}
		});
		return result;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String writeWithDefaultPrettyPrinter(final Object obj) throws PrivilegedActionException{
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			@Override
			public Object run() throws Exception {
				JacksonWrapper.this.writeWithDefaultPrettyPrinterImpl(obj);
				return null;
			}
		});
		return result;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void writeValueToOutputstream(final OutputStream out, final Object obj) throws PrivilegedActionException{
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			@Override
			public Object run() throws Exception {
				JacksonWrapper.this.writeValueToOutputstreamImpl(out, obj);
				return null;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public void writeValueToFilewriter(final FileWriter writer, final Object obj) throws PrivilegedActionException{
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			@Override
			public Object run() throws Exception {
				JacksonWrapper.this.writeValueToFilewriterImpl(writer, obj);
				return null;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public void writeValueToFile(final File file, final Object obj) throws PrivilegedActionException{
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			@Override
			public Object run() throws Exception {
				JacksonWrapper.this.writeValueToFileImpl(file, obj);
				return null;
			}
		});
	}

	private void readCollectionFromFileImpl(final File obj, final Class<?> collection, final Class<?> element){
		try {
			final JavaType type = mapper.getTypeFactory().constructCollectionType((Class<? extends Collection>) collection, element);
			clazz = mapper.readValue(obj,type);
		} catch (final JsonParseException e) {
			e.printStackTrace();
		} catch (final JsonMappingException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	private void readCollectionFromStringImpl(final String obj, final Class<?> collection, final Class<?> element){
		try {
			final JavaType type = mapper.getTypeFactory().constructCollectionType((Class<? extends Collection>) collection, element);
			clazz = mapper.readValue(obj,type);
		} catch (final JsonParseException e) {
			e.printStackTrace();
		} catch (final JsonMappingException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	private void readCollectionFromInputStreamImpl(final InputStream obj, final Class<?> collection, final Class<?> element){
		try {
			final JavaType type = mapper.getTypeFactory().constructCollectionType((Class<? extends Collection>) collection, element);
			clazz = mapper.readValue(obj,type);
		} catch (final JsonParseException e) {
			e.printStackTrace();
		} catch (final JsonMappingException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	private void readValueFromFileImpl(final File obj, final Class<?> cls){
		try {
			clazz = mapper.readValue(obj,cls);
		} catch (final JsonParseException e) {
			e.printStackTrace();
		} catch (final JsonMappingException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	private void readValueFromStringImpl(final String obj, final Class<?> cls){
		try {
			clazz = mapper.readValue(obj,cls);
		} catch (final JsonParseException e) {
			e.printStackTrace();
		} catch (final JsonMappingException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	private void readValueFromInputStreamImpl(final InputStream obj, final Class<?> cls){
		try {
			clazz = mapper.readValue(obj,cls);
		} catch (final JsonParseException e) {
			e.printStackTrace();
		} catch (final JsonMappingException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	private void writeValueAsStringImpl(final Object obj){
		try {
			result = mapper.writeValueAsString(obj);
		} catch (final JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.toString();
		}
	}

	private void writeWithDefaultPrettyPrinterImpl(final Object obj){
		try {
			result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		} catch (final JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.toString();
		}
	}

	private void writeValueToOutputstreamImpl(final OutputStream out, final Object obj) throws IOException{
		try {
			mapper.writeValue(out, obj);
		} catch (final JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	private void writeValueToFilewriterImpl(final FileWriter writer, final Object obj) throws IOException{
		try {
			mapper.writeValue(writer, obj);
		} catch (final JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void writeValueToFileImpl(final File file, final Object obj) throws IOException{
		try {
			mapper.writeValue(file, obj);
		} catch (final JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
